var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO account (email, first_name, last_name) VALUES ?';

    var queryData = [params.email, params.first_name, params.last_name];

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });

};

exports.delete = function(params, callback ) {

    var query = 'DELETE * FROM account WHERE account_id = ?';

    var queryData = [accout_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.edit = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE account SET email = ? WHERE account_id = ?';
    var queryData = [params.account_name, params.account_id];

    connection.query(query, queryData, function (err, result) {
        //delete company_address entries for this company
        companyAddressDeleteAll(params.company_id, function (err, result) {
            callback(err, result);
        });
    });


};